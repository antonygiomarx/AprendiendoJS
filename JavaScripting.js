/*Hola Mundo*/ 
console.log('hello')

/*Variables*/

var ejemplo = 'esto es un string';

var ejemplo2= "esto tambien es un string";

/*Método length*/

ejemplo.length;

/*Revising ("Método replace")*/
ejemplo.replace("Cambie el valor de ejemplo por esto");

/*Números en JS */
var int = 1234
var float = 12.34

/*Método de redondear (Math.round)
*Si el número está por debajo del .5 redondea hacía abajo, si está igual
*o encima del .5 redondea hacía el mayor
*/

var ejemplo3 = 12.3;

Math.round(ejemplo3); // Va a redondear hacía 12 ya que es menor a .5

/*Parse de número a String*/
var numero = 1234;
numero = numero.toString();

/*Estructura If*/
var n=0;
if (n<1) {
    console.log('Hola prro');
    
}

/*Estrucura for*/
for (let i = 0; i < 10; i++) {
    console.log(i);
    
    
}

/*Arrays*/

var mascotas = ['gato', 'perro', 'rata'];

// Filtrado de Arrays
// Va a filtrar todas las mascotas que no sean 'elefante'
var filtrado = mascotas.filter(function (mascotas) {
    return (mascotas !=='elefante')
});

// Para mostrar elemento de un array se sigue el siguiente patrón
console.log(mascotas[0]); //En el índice 0 va a mostrar gato

//Bucle en los arrays

