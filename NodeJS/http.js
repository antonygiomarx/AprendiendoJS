const http = require('http')

/*
http.createServer(function(req, res) {
    res.writeHead(200, {'Content-type':'text/html'});
    res.write('<h1>Hola Mundo desde Nodejs</h1>');
    res.end();
}).listen(3000);
*/
// Esto es de otra manera
/*
const handleServer= function(req, res) {
        res.writeHead(200, {'Content-type':'text/html'});
        res.write('<h1>Hola Mundo desde Node</h1>');
        res.end();
    };

const server = http.createServer(handleServer);

server.listen(3000, function () {
    console.log('Servidor encendido en puerto 3000');
   
    
});*/

/*Ahora creamos el servidor con express*/

const express = require('express');

const server = express();
 
server.get('/', function (req, res) {
  res.send('Hello World con Express');
})
 
server.listen(3000, function(){
    console.log('Servidor encendido en puerto 3000');
    
});