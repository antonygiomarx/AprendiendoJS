const Math = {};

function suma(x, y) {
   return x + y;
}
function resta(x, y) {
    return x - y;
 }
 function multiplicacion(x, y) {
    return x * y;
 }
 function division(x, y) {
    if (y==0) {
        console.log('No se puede dividir entre 0');
        
    } else {
        return x / y;
    }
 }

Math.suma = suma;
Math.resta = resta;
Math.division = division;
Math.multiplicacion = multiplicacion;

module.exports = Math;